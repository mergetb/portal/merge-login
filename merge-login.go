package main

import (
	"encoding/json"
	"flag"
	"fmt"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"

	"github.com/rs/cors"
)

// the following data structures are for unmarshalling kratos login flow response
type LoginResponse struct {
	Methods   Methods
	ExpiresAt string `json:"expires_at"` // when the login session expires.
}
type Methods struct {
	Password Password
}
type Password struct {
	Config PasswordConfig
}
type PasswordConfig struct {
	Fields []Field
}
type Field struct {
	Name     string
	Type     string
	Required bool
	Value    string
}

// LoginResult is the data returned to someone hitting our /login endpoint.
type LoginResult struct {
	Session         string `json:"session"`
	Username        string `json:"username"`
	Email           string `json:"email"`
	AuthenticatedAt string `json:"authenticated_at"`
	ExpiresAt       string `json:"expires_at"`
}

// LoginRequest - data given to our public /login endpoint
type LoginRequest struct {
	Uid string `json:"uid"`
	Pw  string `json:"pw"`
}

// see https://www.ory.sh/kratos/docs/reference/api#check-who-the-current-http-session-belongs-to
type Traits struct {
	Email    string `json:"email"`
	Username string `json:"username"`
}
type Identity struct {
	// There is a URL to the identity schema here. Ideally we'd grab that
	// to parse the traits as the traits are user defined. Luckily we're the
	// ones defining the traits.
	ID     string `json:"id"`
	Traits Traits `json:"traits"`
}
type IDSResp struct {
	AuthenticatedAt string   `json:"authenticated_at"`
	ExpiresAt       string   `json:"expires_at"`
	IssuedAt        string   `json:"issued_at"`
	SID             string   `json:"sid"`
	Identity        Identity `json:"identity"`
}

const (
	PubURL   = "http://127.0.0.1:4433"
	AdminUrl = "http://127.0.0.1:4434"
	version  = "v0.0.1"
)

var (
	LoginFlow = []string{
		PubURL + "/self-service/browser/flows/login?refresh=true",
		AdminUrl + "/self-service/browser/flows/requests/login",
		PubURL + "/self-service/browser/flows/login/strategies/password",
	}
)

func init() {
	val, ok := os.LookupEnv("LOGLEVEL")
	if ok {
		setLogLevel(val)
	}
}

func setLogLevel(val string) {
	lvl, err := log.ParseLevel(val)
	if err != nil {
		log.SetLevel(log.InfoLevel)
	} else {
		log.SetLevel(lvl)
	}
}

func main() {

	portPtr := flag.Int("port", 4455, "port to listen on")
	llPtr := flag.String("loglevel", "", "Level to log at")
	flag.Parse()

	if *llPtr != "" {
		setLogLevel(*llPtr)
	}

	http.HandleFunc("/version", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, version)
	})

	c := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		Debug:          true,
	})

	login := http.HandlerFunc(handleLogin)

	log.Fatal(
		http.ListenAndServe(
			fmt.Sprintf(":%d", *portPtr),
			c.Handler(login),
		),
	)
}

func handleLogin(w http.ResponseWriter, r *http.Request) {

	if r.Method != http.MethodPost {
		http.Error(w, "/login only accepts POST", http.StatusMethodNotAllowed)
		return
	}

	log.Info("/login POST")

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("error reading /login request body")
		http.Error(w, "no request body", http.StatusBadRequest)
		return
	}
	log.Info("/login req body read")

	var lr LoginRequest
	err = json.Unmarshal(body, &lr)
	if err != nil {
		log.Error("unable to unmarshal /login body")
		http.Error(w, "bad request body", http.StatusBadRequest)
		return
	}

	if lr.Uid == "" || lr.Pw == "" {
		log.Error("missing uid or pw in /login")
		http.Error(w, "Missing Form Data", http.StatusBadRequest)
	}

	result, err := login(lr.Uid, lr.Pw)
	if err != nil {
		log.Errorf("login error: %s", err)
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(result)
}

func login(uid, pw string) (*LoginResult, error) {

	// set up the go http client to not automagically follow redirects
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}

	// FLOW PART 1: start the login flow
	resp, err := client.Get(LoginFlow[0])
	if err != nil {
		return nil, fmt.Errorf("request failed: %v", err)
	}

	log.Debugf("FLOW 1 RESP: %#v", resp)

	// extract the csrf cookie (needed in the final step)
	var csrfCookie *http.Cookie
	for _, c := range resp.Cookies() {
		if c.Name == "csrf_token" {
			csrfCookie = c
		}
	}
	if csrfCookie == nil {
		return nil, fmt.Errorf("no crsf cookie found")
	}
	log.Debugf("csrf cookie: " + csrfCookie.Value)

	// extract the request token
	loc, ok := resp.Header["Location"]
	if !ok {
		return nil, fmt.Errorf("no location header")
	}

	locUrl, err := url.Parse(loc[0])
	if err != nil {
		return nil, fmt.Errorf("parse url: %v", err)
	}

	rq, ok := locUrl.Query()["request"]
	if !ok {
		return nil, fmt.Errorf("no request")
	}
	if len(rq) == 0 {
		return nil, fmt.Errorf("empty request")
	}

	log.Debug("request: " + rq[0])

	// FLOW PART 2: continue the login flow using the request token
	resp, err = client.Get(LoginFlow[1] + "?request=" + rq[0])
	if err != nil {
		return nil, fmt.Errorf("request2 failed: %v", err)
	}

	// read the login response json
	out, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("read: %v", err)
	}

	log.Debugf("flow 2 response: \n%#v", resp)
	log.Debugf("flow 2 response body:\n%s\n", string(out))

	var lr LoginResponse
	err = json.Unmarshal(out, &lr)
	if err != nil {
		return nil, fmt.Errorf("parse: %v", err)
	}

	csrf := lr.Methods.Password.Config.Fields[2].Value
	if csrf == "" {
		return nil, fmt.Errorf("missing csrf")
	}
	log.Debugf("csrf: %v", csrf)

	// FLOW PART 3: finish by submitting user credentials
	args := url.Values{
		"identifier": {uid},
		"password":   {pw},
		"csrf_token": {csrf},
	}

	log.Debugf("POSTING: %+v", args)

	req, err := http.NewRequest(
		"POST",
		LoginFlow[2]+"?request="+rq[0],
		strings.NewReader(args.Encode()),
	)
	if err != nil {
		return nil, fmt.Errorf("new post: %v", err)
	}

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	// need to set the csrf cookie (in addition to the csrf token)
	req.Header.Set("Cookie", "csrf_token="+csrfCookie.Value)

	resp, err = client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("post form: %v", err)
	}

	// once we are here there are two ways that i immediately see to tell if the
	// login was successfull.
	//
	//	1. The Location header in the 302
	//		a) if we get redirected back to the auth page, we have failed
	//      b) if we get redirected to the the 'application' page (which i think
	//         is configurable) the login was successful
	//  2. The returned cookies
	//     a) if there are no cookies, the login failed
	//     b) on successfull login we get yet another csrf cookie and also a
	//        ory_kratos_session cookie

	var kratosSession string
	log.Debugf("%#v", resp)
	for _, c := range resp.Cookies() {
		if c.Name == "ory_kratos_session" {
			kratosSession = c.Value
			break
		}
	}

	if kratosSession == "" {
		return nil, fmt.Errorf("Login Failed")
	}

	log.Info("kratos session: %s", kratosSession)

	out, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("flow 3 body %w", err)
	}
	defer resp.Body.Close()

	sInfo, err := readSessionInfo(kratosSession)
	if err != nil {
		return nil, fmt.Errorf("read session info %w", err)
	}

	result := &LoginResult{
		Session:         kratosSession,
		Username:        sInfo.Identity.Traits.Username,
		Email:           sInfo.Identity.Traits.Email,
		AuthenticatedAt: sInfo.AuthenticatedAt,
		ExpiresAt:       lr.ExpiresAt, // note this is the login session experation
	}

	return result, nil
}

func readSessionInfo(token string) (*IDSResp, error) {

	req, err := http.NewRequest("GET", PubURL+"/sessions/whoami", nil)
	if err != nil {
		return nil, fmt.Errorf("auth new IDS request")
	}

	req.Header.Set("Accept", "application/json")
	req.Header.Set("Cookie", "ory_kratos_session="+token)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, fmt.Errorf("IDS GET")
	}

	if resp.StatusCode != 200 {
		log.Debugf("failed resp: %#v", resp)
		return nil, fmt.Errorf("failed to authenticate")
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("bad ids auth resp body")
	}

	log.Debugf("ids resp body: %s", string(body))

	// decode the response
	idsResp := &IDSResp{}
	err = json.Unmarshal(body, idsResp)
	if err != nil {
		log.Errorf("Error parsing IDS auth response %#v", err)
		log.Infof("%s", string(body))
		return nil, err
	}

	return idsResp, nil
}
