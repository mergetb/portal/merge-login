
VERSION = $(shell git describe --always --long --dirty)
LDFLAGS = "-X main.Version=$(VERSION)"

BLUE=\e[34m
GREEN=\e[32m
CYAN=\e[36m
NORMAL=\e[39m

REG ?= quay.io
TAG ?= latest
ORG ?= mergetb

QUIET=@
DEVNULL=>/dev/null
ifeq ($(V),1)
	QUIET=
	DOCKER_QUIET=
	DEVNULL=
endif

all:  merge-login

merge-login: merge-login.go
	@echo "$(BLUE)$@$(GREEN)\t $< $(CYAN)$@$(NORMAL)"
	$(QUIET) go build -ldflags=${LDFLAGS} -o $@ merge-login.go

%-ctr: %.dock merge-login
	@echo "$(BLUE)merge-login-ctr\t $(GREEN)build $(CYAN)$@:$(TAG) $(NORMAL)"
	$(QUIET) docker build --no-cache -f $< -t $(REG)/$(ORG)/merge-login:$(TAG) . $(DEVNULL)
	$(if ${PUSH},$(call docker-push,merge-login))

clean:
	$(QUIET) rm -rf merge-login

define docker-push
	@echo "$(BLUE)merge-login-ctr\t $(GREEN)push $(CYAN)$1:$(TAG) $(NORMAL)"
	$(QUIET) docker push $(REG)$(REGPORT)/$(ORG)/$1:$(TAG) $(DEVNULL)
endef
